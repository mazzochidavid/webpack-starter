const HtmlWebPackPlugin = require('html-webpack-plugin');
const CssWebPackPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin= require('copy-webpack-plugin');
const MinifyPlugin = require("babel-minify-webpack-plugin");


module.exports = {
 
    mode: 'production',
    optimization:{
        minimizer: [new OptimizeCssAssetsPlugin()]
    },
    output:{
        filename:'main.[hash].js',
        clean:true,
    },
    module: {
        rules: [
            {
                test:/\.css$/,
                exclude: /styles\.css$/,
                use:[
                    'style-loader',
                    'css-loader',
                ]
            },
            {
                test:/styles\.css$/,
                use:[
                    CssWebPackPlugin.loader,
                    'css-loader',
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                  loader: "babel-loader",
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
              },
            {
                test:/\.(png|svg|jpg|gif)$/,
                use:[
                    {
                        loader:'file-loader',
                        options: {
                            esModule: false,
                        }
                    }

                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                  loader: "babel-loader",
            }
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: './index.html'
        }),
        new CssWebPackPlugin({
           filename: '[name].[hash].css',
           ignoreOrder:false,
        }),
        new CopyPlugin({
            patterns:[
             { from: 'src/assets', to: 'assets/'}
            ],
        }),
        new MinifyPlugin(),
 
    ] 
};